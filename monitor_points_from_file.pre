###########################################################################################################################
# Script to read points from a file and automatically create monitor points for the coordinates in CFX Pre
#
# ESSS Tech Support
# www.esss.com.br
# esss.zendesk.com
#
#
# INPUT:
#   input_file:          Name of the text file with the list of coordinates (one column for each coordinate - X,Y,Z)
#   file_separator:      Separator used for the coordinates. Use "\t" for tab or " " for single space.
#   file_units:          Units on the input file, enclosed by '[' and ']'.
#   variables:           Variable names to be monitored separated by comma as named by CFX.
#   monitor_point_name:  Name for the monitor points created: "Monitor Point 1", "Monitor Point 2", etc.
#
# USAGE:
#   First, create an input file with the input data (3 columns for the X, Y and Z coordinates). Load this file 
#   (monitor_points_from_file.pre) in CFX-Pre through the menu, in 'Session > Play Session'. The monitor points
#   will be created.
#
# For questions, please contact the ESSS Technical Support.
#
###########################################################################################################################
# INPUT DATA
###########################################################################################################################

! $input_file = "monitor_points_from_file_example.txt";
! $file_separator = "\t";
! $file_units = "[m]";

! $variables = "Temperature, Pressure";

! $monitor_point_name = "Monitor Point";

###########################################################################################################################
# DON'T CHANGE ANYTHING FROM HERE ON UNLESS YOU KNOW WHAT YOU ARE DOING
###########################################################################################################################

COMMAND FILE:
  CFX Pre Version = 15.0
END

! $flows = getChildren( "/", "FLOW" );
! $flows =~ s/FLOW://;
!
! open(FILE, $input_file);
! my $count = 1.0;
! while($tmp = <FILE>)
! {
!   chomp( $tmp );
!   @coords = split($file_separator, $tmp);
!   $x = $coords[0];
!   $y = $coords[1];
!   $z = $coords[2];
!   print "$count - X: $x, Y: $y, Z: $z\n";
!
FLOW: $flows
  OUTPUT CONTROL: 
    MONITOR OBJECTS: 
      MONITOR POINT: $monitor_point_name $count
        Coord Frame = Coord 0
        Option = Cartesian Coordinates
        Cartesian Coordinates = $x $file_units, $y $file_units, $z $file_units
        Output Variables List = $variables
        MONITOR LOCATION CONTROL: 
          Interpolation Type = Nearest Vertex
        END
        POSITION UPDATE FREQUENCY: 
          Option = Initial Mesh Only
        END
      END
    END
  END
END
!
!   $count++;
! }
! close(FILE);
